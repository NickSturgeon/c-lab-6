#ifndef MENU_H
#define MENU_H

/* Function Prototypes */
void main_menu(void);
void display_modify_student_registration_menu(void);
void display_student_menu(void);
void display_course_menu(void);

#endif
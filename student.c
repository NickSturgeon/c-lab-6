#include "student.h"
#include "course.h"
#include <stdio.h>
#include <string.h>

struct student *students;
short num_students = 0;

/* Create / Update Functions */

void add_student(void) {
  short index = num_students++;
  students = realloc(students, num_students * sizeof(struct student));
  struct student new_student;
  enter_student_details(&new_student);
  students[index] = new_student;
  printf("\n");
}

void enter_student_details(struct student *new_student) {
  printf("STUDENT %d\n", num_students);

  short check_error = 0;

  do {
    if (check_error)
      printf("ID already taken or Invalid ID\n");

    printf("Enter student ID: ");
    scanf("%ld", &new_student->student_id);
    while (getchar() != '\n') { continue; } /* clear buffer */

    check_error = check_unique_student_id(new_student->student_id) ||
                  check_student_id_length(new_student->student_id);
  } while (check_error);

  do {
    char first_name[64], last_name[64];
    printf("Enter first name: ");
    scanf("%s", first_name);

    printf("Enter last name:  ");
    scanf("%s", last_name);

    new_student->first_name = malloc(strlen(first_name) + 1);
    new_student->last_name = malloc(strlen(last_name) + 1);
    strcpy(new_student->first_name, first_name);
    strcpy(new_student->last_name, last_name);

  } while (check_error);

  strcpy(new_student->course_id, "");
  new_student->course_marks = 0.0;
}

void register_student(short update) {
  long student_id;
  char course_id[8];

  printf("Enter student ID: ");
  scanf("%ld", &student_id);
  while (getchar() != '\n') { continue; } /* clear buffer */
  struct student *student = get_student_by_id(student_id);

  if (student == NULL) {
    printf("\nNo Match Found\n\n");
    return;
  } else if (strcmp(student->course_id, "") != 0 && !update) {
    printf("\nStudent Already Registered\n\n");
    return;
  }

  printf("Enter course ID: ");
  scanf("%7s", course_id);
  while (getchar() != '\n') { continue; } /* clear buffer */

  if (get_course_by_id(course_id) == NULL)
    printf("\nCourse Does Not Exist\n\n");
  else {
    strcpy(student->course_id, course_id);
    printf("\nCourse Registration Successful\n\n");
  }
}

void remove_student_registration(void) {
  long student_id;
  char course_id[8];

  printf("Enter student ID: ");
  scanf("%ld", &student_id);
  while (getchar() != '\n') { continue; } /* clear buffer */
  struct student *student = get_student_by_id(student_id);

  if (student == NULL) {
    printf("\nNo Match Found\n\n");
    return;
  } else if (strcmp(student->course_id, "") == 0) {
    printf("\nStudent Already Unregistered\n\n");
    return;
  } else {
    strcpy(student->course_id, "");
    printf("\nSuccessfully Removed Registration\n\n");
  }
}

void edit_student_record(void) {
  long student_id;
  short check_error = 0;

  printf("Enter student ID: ");
  scanf("%ld", &student_id);
  while (getchar() != '\n') { continue; } /* clear buffer */
  struct student *student = get_student_by_id(student_id);

  if (student == NULL) {
    printf("\nNo Match Found\n\n");
    return;
  } else if (strcmp(student->course_id, "") == 0) {
    printf("\nStudent Is Not Registered\n\n");
    return;
  }

  do {
    if (check_error)
      printf("Marks Above Course Total or Invalid Marks\n");

    printf("Enter new marks: ");
    if (scanf("%f", &student->course_marks) == 0)
      check_error = 1;
    else if (student->course_marks >
             get_course_by_id(student->course_id)->course_total)
      check_error = 1;
    else
      check_error = 0;

    while (getchar() != '\n') { continue; } /* clear buffer */
  } while (check_error);
}

/* Validation Functions */

short check_unique_student_id(long student_id) {
  short i;
  for (i = 0; i < num_students - 1; i++) {
    if (students[i].student_id == student_id)
      return 1;
  }

  return 0;
}

short check_student_id_length(long id) {
  return id < MIN_ID || id > MAX_ID ? 1 : 0;
}

/* Display Functions */

void display_student(struct student *student) {
  printf("Student ID:   %ld\n", student->student_id);
  printf("Full Name:    %s %s\n", student->first_name, student->last_name);
  if (strcmp(student->course_id, "") == 0) {
    printf("Course Taken: N/A\n");
    printf("Course Marks: N/A\n");
  } else {
    struct course *course = get_course_by_id(student->course_id);
    printf("Course Taken: %s - %s", student->course_id, course->course_title);
    printf("Course Marks: %3.2f / %3.2f\n", student->course_marks,
           course->course_total);
  }

  printf("\n");
}

void display_student_by_id(void) {
  long student_id;

  printf("Enter student ID: ");
  scanf("%ld", &student_id);
  while (getchar() != '\n') { continue; } /* clear buffer */
  printf("\n");

  struct student *student = get_student_by_id(student_id);

  if (student == NULL)
    printf("Student Not Found\n\n");
  else
    display_student(student);
}

void display_students_by_last_name(void) {
  char last_name[64];
  short count = 0;

  printf("Enter last name: ");
  scanf("%63s", last_name);
  while (getchar() != '\n') { continue; } /* clear buffer */
  printf("\n");

  struct student *students_match = get_students_by_last_name(last_name, &count);

  if (count == 0)
    printf("No Students Found\n\n");
  else {
    short i;

    for (i = 0; i < count; i++) {
      display_student(&students_match[i]);
    }
  }

  free(students_match);
}

void display_students_by_first_name(void) {
  char first_name[64];
  short count = 0;

  printf("Enter first name: ");
  scanf("%63s", first_name);
  while (getchar() != '\n') { continue; } /* clear buffer */
  printf("\n");

  struct student *students_match =
      get_students_by_first_name(first_name, &count);

  if (count == 0)
    printf("No Match Found\n\n");
  else {
    short i;

    for (i = 0; i < count; i++) {
      display_student(&students_match[i]);
    }
  }

  free(students_match);
}

void display_students_by_course_id(void) {
  char course_id[8];
  short count = 0;

  printf("Enter course ID: ");
  scanf("%7s", course_id);
  while (getchar() != '\n') { continue; } /* clear buffer */
  printf("\n");

  struct student *students_match = get_students_by_course_id(course_id, &count);

  if (count == 0)
    printf("No Match Found\n\n");
  else {
    short i;

    for (i = 0; i < count; i++) {
      display_student(&students_match[i]);
    }
  }

  free(students_match);
}

void display_all_students(void) {
  short i;

  if (num_students == 0)
    printf("No Students in System\n\n");
  else
    for (i = 0; i < num_students; i++) {
      display_student(&students[i]);
    }
}

void display_unregistered_students(void) {
  short i, match = 0;

  for (i = 0; i < num_students; i++) {
    if (students[i].course_id == 0) {
      match = 1;
      display_student(&students[i]);
    }
  }

  if (!match)
    printf("No Unregistered Students\n\n");
}

/* Search Functions */

struct student *get_student_by_id(long student_id) {
  short i;

  for (i = 0; i < num_students; i++) {
    if (students[i].student_id == student_id)
      return &students[i];
  }

  return NULL;
}

struct student *get_students_by_last_name(char *last_name, short *count) {
  short i;
  struct student *students_match = NULL;

  for (i = 0; i < num_students; i++) {
    if (strcmp(students[i].last_name, last_name) == 0) {
      short index = (*count)++;
      students_match = realloc(students_match, *count * sizeof(struct student));
      students_match[index] = students[i];
    }
  }

  return students_match;
}

struct student *get_students_by_first_name(char *first_name, short *count) {
  short i;
  struct student *students_match = NULL;

  for (i = 0; i < num_students; i++) {
    if (strcmp(students[i].first_name, first_name) == 0) {
      short index = (*count)++;
      students_match = realloc(students_match, *count * sizeof(struct student));
      students_match[index] = students[i];
    }
  }

  return students_match;
}

struct student *get_students_by_course_id(char *course_id, short *count) {
  short i;
  struct student *students_match = NULL;

  for (i = 0; i < num_students; i++) {
    if (strcmp(students[i].course_id, course_id) == 0) {
      short index = (*count)++;
      students_match = realloc(students_match, *count * sizeof(struct student));
      students_match[index] = students[i];
    }
  }

  return students_match;
}

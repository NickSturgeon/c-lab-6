#ifndef STUDENT_H
#define STUDENT_H

#define MIN_ID 10000000
#define MAX_ID 99999999

struct student {
  long student_id;
  char *first_name;
  char *last_name;
  char course_id[8];
  float course_marks;
};

/* Function Prototypes */
void add_student(void);
void enter_student_details(struct student *);
void register_student(short);
void remove_student_registration(void);
void edit_student_record(void);

short check_unique_student_id(long);
short check_student_id_length(long);

void display_student(struct student *);
void display_student_by_id(void);
void display_students_by_last_name(void);
void display_students_by_first_name(void);
void display_students_by_course_id(void);
void display_all_students(void);
void display_unregistered_students(void);

struct student *get_student_by_id(long);
struct student *get_students_by_last_name(char *, short *);
struct student *get_students_by_first_name(char *, short *);
struct student *get_students_by_course_id(char *, short *);

#endif
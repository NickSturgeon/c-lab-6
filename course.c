#include "course.h"
#include "student.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct course *courses;
short num_courses;

/* Create / Update Functions */

void add_course(void) {
  short index = num_courses++;
  courses = realloc(courses, num_courses * sizeof(struct course));
  struct course new_course;
  enter_course_details(&new_course);
  courses[index] = new_course;
  printf("\n");
}

void enter_course_details(struct course *new_course) {
  printf("COURSE %d\n", num_courses);

  short check_error = 0;

  do {
    if (check_error)
      printf("ID already taken or Invalid ID.\n");

    printf("Enter course ID: ");
    scanf("%7s", &new_course->course_id);
    while (getchar() != '\n') { continue; } /* clear buffer */

    check_error = check_unique_course_id(new_course->course_id);
  } while (check_error);

  do {
    char course_title[64];
    printf("Enter course title: ");
    fgets(course_title, 64, stdin);

    new_course->course_title = malloc(strlen(course_title) + 1);
    strcpy(new_course->course_title, course_title);

  } while (check_error);

  do {
    printf("Enter course total: ");
    scanf("%f", &new_course->course_total);
    while (getchar() != '\n') { continue; } /* clear buffer */

    /*check_error = check_student_id_length(new_course->course_id);*/
  } while (check_error);
}

void edit_course_record(void) {
  char course_id[8];
  short check_error = 0;

  printf("Enter course ID: ");
  scanf("%7s", course_id);
  while (getchar() != '\n') { continue; } /* clear buffer */

  struct course *course = get_course_by_id(course_id);

  if (course == NULL) {
    printf("\nCourse Does Not Exist\n\n");
    return;
  }

  do {
    if (check_error)
      printf("Course Total Less Than Student Marks or Invalid Total\n");

    printf("Enter new total: ");
    if (scanf("%f", &course->course_total) == 0)
      check_error = 1;
    else if (check_course_total_value(course_id, course->course_total))
      check_error = 1;
    else
      check_error = 0;

    while (getchar() != '\n') { continue; } /* clear buffer */
  } while (check_error);
}

/* Display Functions */

void display_course(struct course *course) {
  printf("Course ID:    %s\n", course->course_id);
  printf("Course Title: %s", course->course_title);
  printf("Course Total: %3.2f\n", course->course_total);
  printf("\n");
}

void display_course_by_id(void) {
  char course_id[8];

  printf("Enter course ID: ");
  scanf("%7s", course_id);
  while (getchar() != '\n') { continue; } /* clear buffer */
  printf("\n");

  struct course *course = get_course_by_id(course_id);

  if (course == NULL)
    printf("Course Not Found\n\n");
  else
    display_course(course);
}

void display_course_by_student_id(void) {
  long student_id;

  printf("Enter student ID: ");
  scanf("%ld", &student_id);
  while (getchar() != '\n') { continue; } /* clear buffer */
  printf("\n");

  struct student *student = get_student_by_id(student_id);

  if (student == NULL)
    printf("Student Not Found\n\n");
  else {
    struct course *course = get_course_by_id(student->course_id);

    if (course == NULL)
      printf("Student Is Not Registered\n\n");
    else
      display_course(course);
  }
}

void display_courses_by_total(void) {
  float course_total;
  short count;

  printf("Enter course total: ");
  scanf("%f", &course_total);
  while (getchar() != '\n') { continue; } /* clear buffer */
  printf("\n");

  struct course *courses_match = get_courses_by_total(course_total, &count);

  if (count == 0)
    printf("No Match Found\n\n");
  else {
    short i;

    for (i = 0; i < count; i++) {
      display_course(&courses_match[i]);
    }
  }

  free(courses_match);
}

void display_all_courses(void) {
  short i;

  if (num_courses == 0)
    printf("No Courses in System\n\n");
  else
    for (i = 0; i < num_courses; i++) {
      display_course(&courses[i]);
    }
}

void display_unregistered_courses(void) {
  short i, count, match = 0;

  for (i = 0; i < num_courses; i++) {
    get_students_by_course_id(courses[i].course_id, &count);
    if (count == 0) {
      match = 1;
      display_course(&courses[i]);
    }
  }

  if (!match)
    printf("All courses have students registered\n\n");
}

/* Validation Functions */

short check_unique_course_id(char *course_id) {
  short i;
  for (i = 0; i < num_courses - 1; i++) {
    if (strcmp(courses[i].course_id, course_id) == 0)
      return 1;
  }

  return 0;
}

short check_course_total_value(char *course_id, float course_total) {
  short i, count = 0;
  struct student *students_match = get_students_by_course_id(course_id, &count);

  for (i = 0; i < count; i++) {
    if (students_match[i].course_marks > course_total)
      return 1;
  }

  return 0;
}

/* Search Functions */

struct course *get_course_by_id(char *course_id) {
  short i;

  for (i = 0; i < num_courses; i++) {
    if (strcmp(courses[i].course_id, course_id) == 0)
      return &courses[i];
  }

  return NULL;
}

struct course *get_courses_by_total(float course_total, short *count) {
  short i;
  struct course *courses_match = NULL;

  for (i = 0; i < num_courses; i++) {
    if (courses[i].course_total == course_total) {
      short index = (*count)++;
      courses_match = realloc(courses_match, *count * sizeof(struct course));
      courses_match[index] = courses[i];
    }
  }

  return courses_match;
}

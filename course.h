#ifndef COURSE_H
#define COURSE_H

struct course {
  char course_id[8];
  char *course_title;
  float course_total;
};

/* Function Prototypes */
void add_course(void);
void enter_course_details(struct course *);
void edit_course_record(void);

void display_course(struct course *);
void display_course_by_id(void);
void display_course_by_student_id(void);
void display_courses_by_total(void);
void display_all_courses(void);
void display_unregistered_courses(void);

short check_unique_course_id(char *);
short check_course_total_value(char *, float);

struct course *get_course_by_id(char *);
struct course *get_courses_by_total(float, short *);

#endif
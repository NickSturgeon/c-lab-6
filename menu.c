#include "menu.h"
#include "course.h"
#include "student.h"
#include <stdio.h>

void main_menu(void) {
  char choice;

  for (;;) {
    printf("\nSTUDENT COURSE PROGRAM\n");
    printf("1: Add new student\n");
    printf("2: Add new course\n");
    printf("3: Register a student to a course\n");
    printf("4: Modify student registration\n");
    printf("5: Edit student record\n");
    printf("6: Edit course record\n");
    printf("7: Display student record\n");
    printf("8: Display course record\n");
    printf("Q: Quit\n");
    printf("\nEnter choice: ");

    scanf(" %c", &choice);
    while (getchar() != '\n') { continue; } /* clear buffer */
    printf("\n");

    switch (choice) {
    case '1':
      add_student();
      break;
    case '2':
      add_course();
      break;
    case '3':
      register_student(0);
      break;
    case '4':
      display_modify_student_registration_menu();
      break;
    case '5':
      edit_student_record();
      break;
    case '6':
      edit_course_record();
      break;
    case '7':
      display_student_menu();
      break;
    case '8':
      display_course_menu();
      break;
    case 'q':
    case 'Q':
      printf("Quitting...\n");
      return;
    default:
      printf("Invalid Option\n\n");
      break;
    }
  }
}

void display_modify_student_registration_menu(void) {
  char choice;

  for (;;) {
    printf("\nMODIFY STUDENT REGISTRATION\n");
    printf("1: Change course registration\n");
    printf("2: Remove course registration\n");
    printf("B: Go Back\n");
    printf("\nEnter choice: ");

    scanf(" %c", &choice);
    while (getchar() != '\n') { continue; } /* clear buffer */
    printf("\n");

    switch (choice) {
    case '1':
      register_student(1);
      return;
    case '2':
      remove_student_registration();
      return;
    case 'b':
    case 'B':
      return;
    default:
      printf("Invalid Option\n\n");
      break;
    }
  }
}

void display_student_menu(void) {
  char choice;

  for (;;) {
    printf("\nDISPLAY STUDENT RECORD\n");
    printf("1: Display by Student ID\n");
    printf("2: Display by Last Name\n");
    printf("3: Display by First Name\n");
    printf("4: Display by Registered Course ID\n");
    printf("5: Display All Students\n");
    printf("6: Display All Unregistered Students\n");
    printf("B: Go Back\n");
    printf("\nEnter choice: ");

    scanf(" %c", &choice);
    while (getchar() != '\n') { continue; } /* clear buffer */
    printf("\n");

    switch (choice) {
    case '1':
      display_student_by_id();
      return;
    case '2':
      display_students_by_last_name();
      return;
    case '3':
      display_students_by_first_name();
      return;
    case '4':
      display_students_by_course_id();
      return;
    case '5':
      display_all_students();
      return;
    case '6':
      display_unregistered_students();
      return;
    case 'b':
    case 'B':
      return;
    default:
      printf("Invalid Option\n\n");
      break;
    }
  }
}

void display_course_menu(void) {
  char choice;

  for (;;) {
    printf("\nDISPLAY COURSE RECORD\n");
    printf("1: Display by Course ID\n");
    printf("2: Display by Student ID\n");
    printf("3: Display by Course Total\n");
    printf("4: Display All Courses\n");
    printf("5: Display All Courses Without Students\n");
    printf("B: Go Back\n");
    printf("\nEnter choice: ");

    scanf(" %c", &choice);
    while (getchar() != '\n') { continue; } /* clear buffer */
    printf("\n");

    switch (choice) {
    case '1':
      display_course_by_id();
      return;
    case '2':
      display_course_by_student_id();
      return;
    case '3':
      display_courses_by_total();
      return;
    case '4':
      display_all_courses();
      return;
    case '5':
      display_unregistered_courses();
      return;
    case 'b':
    case 'B':
      return;
    default:
      printf("Invalid Option\n\n");
      break;
    }
  }
}

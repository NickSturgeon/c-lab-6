CC = gcc
CC_FLAGS = -pedantic -ansi -Wall -w
OUT_EXE = lab6
FILES = menu.c student.c course.c main.c

build:
	$(CC) $(CC_FLAGS) -o $(OUT_EXE) $(FILES)

clean: 
	rm -rf $(OUT_EXE)

rebuild: clean build
	
run:
	./lab6
